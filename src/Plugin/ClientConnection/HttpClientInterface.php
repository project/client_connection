<?php

namespace Drupal\client_connection\Plugin\ClientConnection;

use GuzzleHttp\ClientInterface;

/**
 * Defines an interface for Client Connection plugins.
 */
interface HttpClientInterface extends ClientInterface {

}
